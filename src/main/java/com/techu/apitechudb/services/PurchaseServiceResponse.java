package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.http.HttpStatus;

public class PurchaseServiceResponse {

    private String msg;
    private PurchaseModel purchaseModel;
    private HttpStatus responseHttpStatusCode;

    public PurchaseServiceResponse() {
    }

    public PurchaseServiceResponse (String msg, PurchaseModel purchaseModel, HttpStatus responseHttpStatusCode)
    {
        this.msg = msg;
        this.purchaseModel = purchaseModel;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PurchaseModel getPurchaseModel() {
        return purchaseModel;
    }

    public void setPurchaseModel(PurchaseModel purchaseModel) {
        this.purchaseModel = purchaseModel;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}

