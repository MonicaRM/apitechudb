package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    //Obtener todas las compras
    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseService");

        return this.purchaseRepository.findAll();

    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findId en prodctuService");

        return this.purchaseRepository.findById(id);
    }

    //public PurchaseServiceResponse addPurchase(PurchaseModel purchase){
    public PurchaseServiceResponse addPurchase(PurchaseModel purchase){
        System.out.println("addpruchase en pruchase");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchaseModel(purchase);

        if(this.userService.findById(purchase.getUserId()).isPresent() == false){
            System.out.println("El usuario de la compra no se ha encontrado ");

            result.setMsg("El usuario de la compra no se ha encontrado ");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.findById(purchase.getId()).isPresent() == true){
            System.out.println("Ya hay una compra con esas id ");

            result.setMsg("Ya hay una compra con esas id ");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;

        }

        float amount = 0;
        for(Map.Entry<String,Integer> purchaseItem : purchase.getpurchaseItems().entrySet()){
            if(this.productService.findById(purchaseItem.getKey()).isPresent() == false){
                System.out.println("El producto con la id  "+ purchaseItem.getKey() + "no se encuentra en el sistema");
                result.setMsg("El producto con la id  "+ purchaseItem.getKey() + "no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            }else{
                System.out.println("Añadiendo valor del  " + purchaseItem.getValue() + "unidades del producto al total ");

                amount += (this.productService.findById(purchaseItem.getKey()).get().getPrice() * purchaseItem.getValue());
            }

        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correcta");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;

    }
}
