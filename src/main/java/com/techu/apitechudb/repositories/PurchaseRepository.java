package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    //Obtener todas las compras
    public List<PurchaseModel> findAll(){
        System.out.println("findAll en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }

    public Optional<PurchaseModel> findById(String id){
        System.out.println("findById en purcharseRepository");
        System.out.println("la id des " + id);

        Optional<PurchaseModel> result  = Optional.empty();

        for (PurchaseModel pruchaseInList : ApitechudbApplication.purchaseModels){
            if(pruchaseInList.getId().equals(id)){
                System.out.println("Compra con id" + id + "encontrada");
                result = Optional.of(pruchaseInList);

            }
            return result;
        }
        return result;
    }

    public PurchaseModel save(PurchaseModel purchaseModel) {
        System.out.println("Save en product repository");

        ApitechudbApplication.purchaseModels.add(purchaseModel);

        return purchaseModel;
    }

}
