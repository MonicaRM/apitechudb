package com.techu.apitechudb.Controller;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")

public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUser(@RequestParam (value = "age", defaultValue = "0") int age){
        System.out.println("getUser");

        return new ResponseEntity<>(
                this.userService.findAll(age),
                HttpStatus.OK
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuerio a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("El id del empleado a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id){
        System.out.println("La id del usuario que se va actualizar por parametro es " + id);
        System.out.println("El id del usuario que se va actualizar es " + user.getId());
        System.out.println("El nombre del usuario  que se va actualizar es " + user.getName());
        System.out.println("La edad del usuario que se va actualizar es " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);

    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuerio a borrar es " + id);

        boolean deleteProduct = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Usuario borrado" : "Usuario no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

}
