package com.techu.apitechudb.Controller;


import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    //Obtener todas las compras
    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases(){
        System.out.println("getPurchse");

        return new ResponseEntity<>(this.purchaseService.findAll(),HttpStatus.OK);
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase (@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");
        System.out.println("La id de purchase a crear es " + purchase.getId());
        System.out.println("El userId de purchase a crear es " + purchase.getUserId());
        System.out.println("El amount de purchase a crear es " + purchase.getAmount());
        System.out.println("Los purchaseItems a crear son " + purchase.getpurchaseItems());

        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                purchaseServiceResponse.getPurchaseModel(),
                purchaseServiceResponse.getResponseHttpStatusCode()
        );
    }

}
