package com.techu.apitechudb.models;

import java.util.Map;

public class PurchaseModel {

    private String id;
    private String userId;
    private Float amount;
    private Map<String,Integer> purchaseItems;

    public PurchaseModel() {

    }

    public void PurchaseItems(){

    }

    public PurchaseModel(String id, String userId, Float amount,  Map<String,Integer> purchaseItems){
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getpurchaseItems() {
        return purchaseItems;
    }

    public void setpurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
